Name:           libzapojit
Version:        0.0.3
Release:        16
Summary:        GLib/GObject wrapper for the SkyDrive and Hotmail REST APIs
License:        LGPLv2+
URL:            https://wiki.gnome.org/Projects/Zapojit
Source0:        http://download.gnome.org/sources/libzapojit/0.0/libzapojit-%{version}.tar.xz

Patch0001:      CVE-2021-39360.patch

BuildRequires:  gettext pkgconfig(gio-2.0) >= 2.28 pkgconfig(glib-2.0) >= 2.28
BuildRequires:  pkgconfig(gobject-2.0) >= 2.28 pkgconfig(goa-1.0)
BuildRequires:  pkgconfig(gobject-introspection-1.0) gtk-doc intltool
BuildRequires:  pkgconfig(json-glib-1.0) pkgconfig(libsoup-2.4) >= 2.38
BuildRequires:  libtool pkgconfig(rest-0.7)
Requires:       gobject-introspection

%description
GLib/GObject wrapper for the SkyDrive and Hotmail REST APIs.

%package        devel
Summary:        The development file for libzapojit
Requires:       gobject-introspection-devel %{name} = %{version}-%{release}

%description    devel
The libzapojit-devel package contains libraries and header files for
developing applications.

%prep
%autosetup -n libzapojit-%{version} -p1

%build
%configure --disable-silent-rules --enable-gtk-doc --enable-introspection
sed --in-place --expression 's! -shared ! -Wl,--as-needed\0!g' libtool
%make_build

%install
%make_install
%delete_la

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/libzapojit-0.0.so.*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/Zpj-0.0.typelib

%files devel
%{_libdir}/libzapojit-0.0.so
%{_libdir}/pkgconfig/zapojit-0.0.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Zpj-0.0.gir
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%doc %{_datadir}/gtk-doc/html/libzapojit-0.0
%dir %{_includedir}/libzapojit-0.0
%{_includedir}/libzapojit-0.0/zpj
%exclude %{_libdir}/libzapojit-0.0.a
%exclude %{_datadir}/doc/libzapojit

%changelog
* Fri Nov 12 2021 yaoxin <yaoxin30@huawei.com> - 0.0.3-16
- Fix CVE-2021-39360

* Tue Jun 9 2020 leiju <leiju4@huawei.com> - 0.0.3-15
- Package init
